package com.sunmnet.current.anniationas;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;



import lombok.extern.slf4j.Slf4j;


@Slf4j
public class ConcurrencyTest{
    
	public static int clientTotal=5000;
	
	public static int threadTotal=50;
	
	public static int count=0;
	
	public static void main(String[] args) throws InterruptedException {
    	
    	ExecutorService executorService=Executors.newCachedThreadPool();
    	
    	final Semaphore semaphore=new Semaphore(threadTotal);
    	
    	final CountDownLatch countDownLatch=new CountDownLatch(clientTotal);
    	
    	for (int i = 0; i < clientTotal; i++) {
    		executorService.execute(()->{
    			try {
					semaphore.acquire();
					add();
	    			semaphore.release();
				} catch (InterruptedException e) {					
					log.error("exception",e);
				} 
    			countDownLatch.countDown();
    		});
			
		}
    	
    	countDownLatch.await();    	
    	executorService.shutdown();
        log.info("count:{}",count);  	
    	
    }
    
    
    private static void add() {
    	count++;
  }
    
}
