package com.sunmnet.current.controller;


import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/test")
@Slf4j
public class TestController {
	
	@PostMapping("/test")
	public String  test(){
		log.info("test");
		return  "test";
	}
	

}
