package com.sunmnet.current.controller;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.sunmnet.current.aqs.threadlocal.RequestHolder;

@RestController
@RequestMapping("/threadLocal")
public class ThreadLocalController {

	
	@PostMapping("/test")
	public long  test() {
		
		return RequestHolder.getId();
	}
}
