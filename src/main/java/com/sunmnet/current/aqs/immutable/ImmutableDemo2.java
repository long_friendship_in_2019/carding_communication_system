package com.sunmnet.current.aqs.immutable;

import java.util.Collections;
import java.util.Map;

import com.google.common.base.FinalizablePhantomReference;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Maps;
import com.sunmnet.current.annoations.ThreadSafe;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@ThreadSafe
public class ImmutableDemo2 {

	private final static ImmutableList<Integer> list=ImmutableList.of(1,2,3,4);
	
	private final static ImmutableSet<Integer> set=ImmutableSet.copyOf(list);
	
	private final static ImmutableMap<Integer,Integer> map=ImmutableMap.of(1,2,3,4,5,6);
	
	private final static ImmutableMap<Integer,Integer> map2=ImmutableMap.<Integer,Integer>builder().put(1,2).put(3,4).put(5,6).build();
			
 	
	
	public static void main(String[] args) {
	

		
		log.info("{}",map2.get(3));
	}
	
	
	

}
