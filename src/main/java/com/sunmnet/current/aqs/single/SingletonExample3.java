package com.sunmnet.current.aqs.single;

import com.sunmnet.current.annoations.NotThreadSafe;
import com.sunmnet.current.annoations.ThreadSafe;

import lombok.extern.slf4j.Slf4j;

/**
 * 懒汉模式的实现
 * @author Administrator
 *
 */
@ThreadSafe
@Slf4j
public class SingletonExample3 {
   
	private static SingletonExample3 instace=null;
	
	private SingletonExample3() {
		
	}
	
	//静态工厂方法
	public static synchronized SingletonExample3 getInstance() {
		
		if (instace==null) {
			instace=new SingletonExample3();
		}
		
		return instace;
	}
	
	
}
