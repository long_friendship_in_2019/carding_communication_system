package com.sunmnet.current.aqs.single;

import com.sunmnet.current.annoations.NotThreadSafe;

import lombok.extern.slf4j.Slf4j;

/**
 * 懒汉模式的实现
 * @author Administrator
 *
 */
@NotThreadSafe
@Slf4j
public class SingletonExample {
   
	private static SingletonExample instace=null;
	
	private SingletonExample() {
		
	}
	
	
	public static SingletonExample getInstance() {
		
		if (instace==null) {
			
			instace=new SingletonExample();
		}
		
		return instace;
	}
	
	
}
