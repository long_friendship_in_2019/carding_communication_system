package com.sunmnet.current.aqs.single;

import com.sunmnet.current.annoations.NotThreadSafe;
import com.sunmnet.current.annoations.ThreadSafe;

import lombok.extern.slf4j.Slf4j;

/**
 * 懒汉模式的实现
 * @author Administrator
 *
 */
@ThreadSafe
@Slf4j
public class SingletonExample2 {
   
	private static SingletonExample2 instace=new SingletonExample2();
	
	private SingletonExample2() {
		
	}
	
	
	public static SingletonExample2 getInstance() {		
		return instace;
	}
	
	
}
