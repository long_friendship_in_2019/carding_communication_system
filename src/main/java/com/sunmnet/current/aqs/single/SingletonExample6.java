package com.sunmnet.current.aqs.single;

import com.sunmnet.current.annoations.NotThreadSafe;
import com.sunmnet.current.annoations.ThreadSafe;

import lombok.extern.slf4j.Slf4j;

/**
 * 懒汉模式的实现
 * @author Administrator
 *
 */
@ThreadSafe
@Slf4j
public class SingletonExample6 {
	
	
    /**
     *  一定要注意static 静态作用域与 定义变量的先后顺序
     */
	private static SingletonExample6 instace=null;
	
	static {
		instace=new SingletonExample6();
	}
	
	private SingletonExample6() {
		
	}
	
	
	public static SingletonExample6 getInstance() {		
		return instace;
	}
	
	public static void main(String[] args) {
		System.out.println(SingletonExample6.getInstance());
		
	}
	
	
}
