package com.sunmnet.current.aqs.single;

import com.sunmnet.current.annoations.ThreadSafe;

import lombok.extern.slf4j.Slf4j;

/**
 * 懒汉模式的实现
 * @author Administrator
 *
 */
@ThreadSafe
@Slf4j
public class SingletonExample4 {
   
	private static volatile SingletonExample4 instace=null;
	
	private SingletonExample4() {
		
	}
	
	/**
	 * 1.memoryallocate() 分配对象内存空间
	 * 2.ctorInstance() 初始化对象
	 * 3.instance=mkemory 设置instance 指向刚分配内存的
	 * 
	 * jvm 与 cpu的指令重排 导致双重检测机制重拍
	 * cpu 会发现指令重排  2 3
	 * @return
	 */
	
	public static SingletonExample4 getInstance() {
		
		if (instace==null) {
			synchronized (SingletonExample4.class) {
				if (instace==null) {//双重检测机制以及加锁的方法
					instace=new SingletonExample4();
				}				
			}			
		}		
		return instace;
	}
	
	
}
