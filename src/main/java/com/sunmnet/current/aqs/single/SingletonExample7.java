package com.sunmnet.current.aqs.single;

import com.sunmnet.current.annoations.NotThreadSafe;
import com.sunmnet.current.annoations.Recommend;
import com.sunmnet.current.annoations.ThreadSafe;

import lombok.extern.slf4j.Slf4j;

/**
 * 枚举模式:最安全的,不用做其他的处理
 * @author Administrator
 *
 */
@ThreadSafe
@Slf4j
@Recommend
public class SingletonExample7 {
   
	
	
	private SingletonExample7() {
		
	}
	
	
	public static SingletonExample7 getInstance() {
				
		return Singleton.INSTANCE.getInstance();
	}
	
	private enum Singleton{
		INSTANCE;
		
		private SingletonExample7  singleton;
		// JVM 保证 只会调用一次.....
		private Singleton() {
			singleton=new SingletonExample7();
		}
		
		public SingletonExample7 getInstance() {
			return singleton;
		}
		
	}
	
	
}
