package com.sunmnet.current.aqs.commonUnsafe;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;

import com.sunmnet.current.annoations.NotThreadSafe;
import com.sunmnet.current.annoations.ThreadSafe;

import lombok.extern.slf4j.Slf4j;

@ThreadSafe
@Slf4j
public class SimpleDateFormatExample2 {
	
	// 请求总数
    public static int clientTotal = 5000;

    // 同时并发执行的线程数
    public static int threadTotal = 200;
    //不是线程安全的
    

    public static void main(String[] args) throws Exception {
        ExecutorService executorService = Executors.newCachedThreadPool();
        final Semaphore semaphore = new Semaphore(threadTotal);
        final CountDownLatch countDownLatch = new CountDownLatch(clientTotal);
        for (int i = 0; i < clientTotal ; i++) {
            executorService.execute(() -> {
                try {
                    semaphore.acquire();
                    update();
                    semaphore.release();
                } catch (Exception e) {
                    log.error("exception", e);
                }
                countDownLatch.countDown();
            });
        }
        countDownLatch.await();
        executorService.shutdown();
        log.info("str length:{}");
    }

    private static void update() {
       try {
    	  //每次再来一个新的类
    	SimpleDateFormat format=new SimpleDateFormat("yyyyMMdd") ;
		format.parse("20190827");
	} catch (ParseException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
    }
	

}
