package com.sunmnet.current.aqs.commonUnsafe;

import java.util.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import com.sunmnet.current.annoations.NotThreadSafe;

import lombok.extern.slf4j.Slf4j;

@NotThreadSafe
@Slf4j
public class SimpleDateFormatExample3 {
	
	// 请求总数
    public static int clientTotal = 5000;

    // 同时并发执行的线程数
    public static int threadTotal = 200;
    //不是线程安全的
    public static  DateTimeFormatter format=DateTimeFormat.forPattern("yyyyMMdd") ;

    public static void main(String[] args) throws Exception {
        ExecutorService executorService = Executors.newCachedThreadPool();
        final Semaphore semaphore = new Semaphore(threadTotal);
        final CountDownLatch countDownLatch = new CountDownLatch(clientTotal);
        for (int i = 0; i < clientTotal ; i++) {
        	final int count=i;
            executorService.execute(() -> {
                try {
                    semaphore.acquire();
                    update(count);
                    semaphore.release();
                } catch (Exception e) {
                    log.error("exception", e);
                }
                countDownLatch.countDown();
            });
        }
        countDownLatch.await();
        executorService.shutdown();
        log.info("str length:{}");
    }

    private static void update(int i) {
      
    	Date date=DateTime.parse("20190827",format).toDate();		
    	log.info("{},{}",i,date);
    }
	

}
