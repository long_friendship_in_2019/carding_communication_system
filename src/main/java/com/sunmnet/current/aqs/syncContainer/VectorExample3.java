package com.sunmnet.current.aqs.syncContainer;

import java.util.Iterator;
import java.util.List;
import java.util.Vector;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class VectorExample3 {

	

	public static void main(String[] args) throws Exception {
         
		Vector<Integer> vector=new Vector<>();
		
		vector.add(1);
		vector.add(2);
		vector.add(3);
		vector.add(4);
		
		
		//test1(vector);
		test2(vector);
		//System.out.println(vector);
		//test3(vector);
		
	}

	//java.util.ConcurrentModificationException
	private static void test1(Vector<Integer> vector) {
		for (Integer i:vector) {
			if (i==3) {
				vector.remove(i);
				System.out.println(i);
			}			
		}		
	}
	
	//java.lang.ArrayIndexOutOfBoundsException: Array index out of range: 3
	private static void test2(Vector<Integer> vector) {
		Iterator<Integer> iterator = vector.iterator();
		while (iterator.hasNext()) {
			int c=iterator.next();
			if (c==3) {
				//vector.remove(c);
				iterator.remove();
				System.out.println(c);
			}	
		}	
	}
	
	
	private static void test3(Vector<Integer> vector) {
		for (int i = 0; i < vector.size(); i++) {
			if (vector.get(i)==3) {
				vector.remove(i);
			}			
		}		
	}
	
	

}
