package com.sunmnet.current.aqs.syncContainer;

import java.util.List;
import java.util.Vector;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class VectorExample2 {

	private static Vector<Integer> vector = new Vector<>();
	// 请求总数
	public static int clientTotal = 5000;

	// 同时并发执行的线程数
	public static int threadTotal = 200;

	private static List<Integer> list = new Vector<>();

	public static void main(String[] args) throws Exception {
        int count=0;
		while (count<100) {
			for (int i = 0; i < 10; i++) {
				vector.add(i);
			}
			
			Thread thread1=new Thread() {
				@Override
				public void run() {
					for (int i = 0; i < vector.size(); i++) {
						vector.remove(i);
					}
					
				}				
			};
			
			Thread thread2=new Thread() {				
				@Override
				public void run() {
					for (int i = 0; i < vector.size(); i++) {
						vector.get(i);
					}
				}
			};
			

			
			thread1.start();
			thread2.start();
			count++;
		}

	}

}
