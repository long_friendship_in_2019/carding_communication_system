package com.sunmnet.current.aqs.publish;

import java.util.Arrays;

import com.sunmnet.current.annoations.NotThreadSafe;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@NotThreadSafe
@Data
public class UnsafePublish {

	
	private String[] states= {"a","b","c","d","e"};
	
	public static void main(String[] args) {
		UnsafePublish unsafePublish=new UnsafePublish();
		log.info("states:{}",Arrays.toString(unsafePublish.getStates()));
		unsafePublish.getStates()[0]="d";
		log.info("states:{}",Arrays.toString(unsafePublish.getStates()));
		
	}
	
}
