package com.sunmnet.current.aqs.automic;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;
import java.util.concurrent.atomic.AtomicLong;

import com.sunmnet.current.annoations.ThreadSafe;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@ThreadSafe
public class ActomicLongDemo1 {
	
	
	private static int clinetTotal=5000;
	
	private static int threadTotal=200;
	
	private static AtomicLong count=new AtomicLong(0);
	

	
	public static void main(String[] args) throws Exception{
		
		ExecutorService executorService=Executors.newCachedThreadPool();
		
		final Semaphore semaphore=new Semaphore(threadTotal);
		
		final CountDownLatch countDownLatch=new CountDownLatch(clinetTotal);
		
		for (int i = 0; i < clinetTotal; i++) {
			executorService.execute(()->{
				try {
					
					semaphore.acquire();
					count();
					semaphore.release();
				} catch (InterruptedException e) {					
					e.printStackTrace();
				}
				countDownLatch.countDown();		
				
			});
			
		}
		
		countDownLatch.await();
		
	    executorService.shutdown();
	    
	    log.info("count:{}",count);

	}



	private static void count() {
		count.getAndIncrement();		
	}
	
	

}
