package com.sunmnet.current.aqs.automic;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;
import java.util.concurrent.atomic.AtomicReference;
import java.util.concurrent.atomic.LongAdder;

import com.sunmnet.current.annoations.ThreadSafe;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@ThreadSafe
public class ActomicLongDemo3 {
	
	
  private static AtomicReference<Integer> count=new AtomicReference<>(0);
	

	
	public static void main(String[] args) throws Exception{
		
		count.compareAndSet(0, 2);		
	    log.info("count:{}",count.get());
	    count.compareAndSet(1, 4);	//不发生改变 no
	    count.compareAndSet(3, 5);	//不发生改变
	    log.info("count:{}",count.get());
	    count.compareAndSet(2, 5);
	    log.info("count:{}",count.get());
	    count.compareAndSet(7, 8);//no
	    log.info("count:{}",count.get());

	}



	

}
