package com.sunmnet.current;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CommuncationApplication {

	public static void main(String[] args) {
		SpringApplication.run(CommuncationApplication.class, args);
	}

}
