package com.sunmnet.current.config;

import javax.servlet.Filter;


import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import com.sunmnet.current.config.filter.HttpFilter;
import com.sunmnet.current.config.interceptor.HttpInterceptor;

@SuppressWarnings("deprecation")
@Configuration
public class ServletConfig extends WebMvcConfigurerAdapter{
	
	
	@Bean
	public  FilterRegistrationBean<Filter> httpFilter(){
		FilterRegistrationBean<Filter> filter=new FilterRegistrationBean<>();
		
		filter.setFilter(new HttpFilter());
		filter.addUrlPatterns("/*");		
		
		return filter;		
	}
	
	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		registry.addInterceptor(new HttpInterceptor())
		        .addPathPatterns("/**");
	}
	
	

}
