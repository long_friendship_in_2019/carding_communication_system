package com.sunmnet.current.model;

import lombok.Data;

@Data
public class SysUser {

	private String id;
	
	private String name;
	private String account;
	
	private String sex;
	private String phone;
	
	
}
